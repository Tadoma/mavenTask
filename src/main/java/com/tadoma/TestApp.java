package com.tadoma;

import org.junit.gen5.api.Test;
import static org.junit.gen5.api.Assertions.assertEquals;

public class TestApp {
    String message = "Hello World";
    App messageUtil = new App(message);

    @Test
    public void testPrintMessage() {
        assertEquals(message,messageUtil.printMessage());
    }
}
