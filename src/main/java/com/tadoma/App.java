package com.tadoma;

public class App 
{
    private String message;

    public App(String message){
        this.message = message;
    }

    public String printMessage(){
        System.out.println(message);
        return message;
    }

    public static void main( String[] args ) {
        TestApp test = new TestApp();
        test.testPrintMessage();
    }
}
